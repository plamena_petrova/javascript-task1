//Task 11
//Given an amount, return '<amount> leva', except add '($$$)' at the end if the amount is 1 million.
//For example:
//chekValue(10)
//result 10 leva
//chekValue(42)
//result 42 leva
//chekValue(1000000)
//result 1000000 dollars ($$$)

function checkValue(value)
{
	var str=value.toString();
	var n=str.length;
	
	if(n < 7){
		return value + " "+"leva";
		}
	else {
			 return value + " " + "dollars ($$$)";
		  }
}
