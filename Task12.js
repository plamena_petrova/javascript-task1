//Task 12
//Given two strings, return the concatenation of the two strings (separated by a space) slicing out
//and swapping the first 2 characters of each. You can assume that the strings are at least 2
//characters long. For example:
//mixUp('mix', 'pod')
//result pox mid
function mixUp(a,b){
	var tmp=a;
	a=b;
	b=tmp;

	var tmpA=a.length;
	var tmpB=b.length;
	
	var newA=a.replace(tmpA-1,tmpB-1);
	var newB=b.replace(tmpB-1, tmpA-1);
	
	return newA + " " + newB;
}
