//Task 13
//Given a string, return a version where all occurrences of its first character have been replaced
//with '*', except do not change the first character itself. You can assume that the string is at least
//one character long. For example: ba*ble

function fixStart()
{
	var str="babble";
	var splitted=str.split("");

	 for(var i=0; i<=splitted.length; i++){
		 if(splitted[i]==splitted[i+1])
		 {
			 var res=str.replace(splitted[i+1], "*");
			 return res;
		 }		
	 }
}