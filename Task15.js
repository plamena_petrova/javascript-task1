//Task 15
//Given a string, find the first appearance of the substring 'not' and 'bad'. If the 'bad' follows the
//'not', replace the whole 'not'...'bad' substring with 'good' and return the result. If you don't find
//'not' and 'bad' in the right sequence (or at all), just return the original sentence. For example:
//'This dinner is not that bad!':
function notBad(str)
{
	var splitted=str.split(" ");
		var not=str.indexOf("not");
		var bad=str.indexOf("bad");
			
	for(var i=0; i<splitted.length;i++)
	{
		if(splitted[i]=="not" && splitted[i]!="bad")
		{
			var cuttedStr=/(not\s+)(.*)(\s+bad.*)/;
			var res=str.replace(cuttedStr, "good");
			return res;
		}
		else if(splitted[i]=="bad" && splitted[i]!="not")
		{
			return str;
		}
	}
}