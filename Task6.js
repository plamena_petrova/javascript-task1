//Task 6
//Define a function reverse() that computes the reversal of a string. For example, reverse("jag
//testar") should return the string "ratset gaj".

function reverse(){
	var rev="jag testar";
	var result= rev.split("").reverse().join("");
	return result;
}